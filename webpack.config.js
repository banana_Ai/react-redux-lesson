var	path = require('path')
var	webpack = require('webpack')

module.exports = {
	devtool: 'cheap-module-eval-source-map',
	entry: [
		'./src/index.js'
	],
	output: {
		path: './dist',
		filename: 'bundle.js',
	},
	plugins: [
		new	webpack.optimize.OccurenceOrderPlugin(),
	],
	module:	{
		preLoaders: [
			{
				loader: 'eslint',
				include: /src/,
				test: /\.js$/
			}
		],
		loaders: [
			{
				loader: 'babel',
				include: /src/,
				test: /\.js$/
			}
		]
	}
}