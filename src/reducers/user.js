import {GET_OAUTH_LINK_REQUEST, GET_OAUTH_LINK_SUCCESS, GET_OAUTH_LINK_FAIL} from '../constants/User'

const initialState = {
	vkId: null,
	name: null,
	token: null,
	oAuthLink: null
};

export default function user(state = initialState, action) {
	switch(action.type){
		case GET_OAUTH_LINK_SUCCESS:
			return {...state, oAuthLink: action.payload}
		case GET_OAUTH_LINK_FAIL:
			return {...state, oAuthLink: null}
		default:
			return state;
	}
}