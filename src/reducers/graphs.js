import QueriesActions from '../constants/Graphs'

class UserNode {
    constructor(vkId, name, sex){
        this._vkId = vkId;
        this._name = name;
        this._sex = sex;
    }

    get vkId() {return this._vkId; }
    get name() {return this._name; }
    get sex() {return this._sex; }

    get id() {return this.vkId; }
    get label() {
        var label = '';
        if(this.name == null)
            label = this.vkId;
        else
            label = this.name;

        if(this.sex != null)
            label += ' ' + this.sex;
        return label;
    }

    get node(){
        return {
            id: this.id,
            label: this.label
        };
    }

    static fromData(data){
        return new UserNode(data.vkId, data.name, data.sex);
    }
}

class UserEdge {
    constructor(vkId1, vkId2){
        this._vkId1 = vkId1;
        this._vkId2 = vkId2;
    }

    get from() {return this._vkId1; }
    get to() {return this._vkId2; }

    get vkId1() {return this._vkId1; }
    get vkId2() {return this._vkId2; }

    get edge() {
        return {
            from: this.from,
            to: this.to
        };
    }

    get hash() {return this.vkId1 ^ this.vkId2;}
}

function getValues(ar){
    return Object.keys(ar).map((k) => {return ar[k]});
}

class UserGraph {
    constructor() {
        this._nodes = {};
        this._edges = {};
    }

    insertNode(node){
        this._nodes[node.vkId] = node;
    }

    insertNodes(nodes){
        var self = this;
        nodes.forEach((node) => {self.insertNode(node)});
    }

    connect(edge){
        this._edges[edge.hash] = edge;
    }

    connectAll(vkId, vkIds){
        var self = this;
        vkIds.forEach((id)=>{self.connect(new UserEdge(vkId, id))});
    }

    get nodes() {return getValues(this._nodes); }
    get edges() {return getValues(this._edges); }

    get data() {
        return {
            nodes: this.nodes.map((n)=>{return n.node}),
            edges: this.edges.map((e)=>{return e.edge})
        };
    }
}

const initialState = {
    userGraph: new UserGraph()
};

export default function queries(state = initialState, action) {
    var userGraph = state.userGraph;
    switch (action.type) {
        case QueriesActions.GET_USER_SUCCESS: {
            userGraph.insertNode(UserNode.fromData(action.payload));
            return {userGraph: userGraph};
        }
        case QueriesActions.GET_FRIENDS_SUCCESS: {
            var userNodes = action.payload.friends.map((data) => {
                return UserNode.fromData(data)
            });
            userGraph.insertNodes(userNodes);
            userGraph.connectAll(action.payload.vkId, userNodes.map((node) => {
                return node.vkId
            }));
            return {userGraph: userGraph};
        }
        default:
            return state;
    }
}