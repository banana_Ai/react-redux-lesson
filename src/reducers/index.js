import { combineReducers } from 'redux'
import user	from './user'
import graphs from './graphs'
import loader from './loader'

export default combineReducers({
	user,
    graphs,
	loader
})