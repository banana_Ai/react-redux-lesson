import React, {Component} from 'react'

class OAuthRequest extends Component {

	onOAuthRequestButtonClick() {
		this.props.getOAuthLink();
	}

	render() {
		return <button onClick={this.onOAuthRequestButtonClick.bind(this)}>Request oauth link</button>;
	}
}

class LoginForm extends Component {
	render() {
		var oAuthLink = this.props.oAuthLink;
		return (<form>
					<a href={oAuthLink} target='_blank'>Login link</a><br />
                    <input type='text'
                        placeholder='secret code'
                        ref='code' 
                        />
                    <button>Login in</button>
				</form>);
	}
}

class UserInfo extends Component {
	render() {
		var user = this.props.user;
		return (<div>{user.name}</div>);
	}
} 

export default class User extends Component {
	render() {
		var user = this.props.user;
		var userActions = this.props.userActions;
		if(user.oAuthLink == null)
			return <OAuthRequest getOAuthLink={userActions.getOAuthLink}/>;
		else if(user.vkId == null)
			return <LoginForm oAuthLink={user.oAuthLink} />
		else
			return <UserInfo user={user} />
	}
}