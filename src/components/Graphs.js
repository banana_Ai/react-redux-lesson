import React, {Component, PropTypes} from 'react'
import ReactDOM from 'react-dom'
import Graph from 'react-graph-vis'

class GraphsList extends Component {
    render() {
        return <div>
            {JSON.stringify(this.props.graphs.userGraph.data)}
            <Graph graph={this.props.graphs.userGraph.data} />
        </div>;
    }
}

class RequestForm extends Component {
    onGetUserButtonClick(e) {
        e.preventDefault();
        var vkIdInput = ReactDOM.findDOMNode(this.refs.vkid);
        var vkId = vkIdInput.value.trim();
        this.props.queriesActions.getUser(vkId);
    }

    onGetFriendsButtonClick(e) {
        e.preventDefault();
        var vkIdInput = ReactDOM.findDOMNode(this.refs.vkid);
        var vkId = vkIdInput.value.trim();
        this.props.queriesActions.getFriends(vkId);
    }

    render() {
        return <form>
            <input type='text'
                   placeholder='user vk id'
                   ref='vkid'
            />
            <button onClick={this.onGetUserButtonClick.bind(this)}>Get user</button>
            <button onClick={this.onGetFriendsButtonClick.bind(this)}>Get friends</button>
        </form>;
    }
}

export default class Graphs extends Component {
	render() {
		return	<div>
            <RequestForm queriesActions={this.props.queriesActions}/>
            <GraphsList graphs={this.props.graphs} />
        </div>;
	}
}