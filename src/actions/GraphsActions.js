import QueriesActions from '../constants/Graphs'

var $ = require('jquery/src/core');
require('jquery/src/ajax');
require('jquery/src/ajax/xhr');

export function getUser(vkId){
    return (dispatch) => {
        dispatch({
            type: QueriesActions.GET_USER_REQUEST
        });

        $.ajax({
            method: 'GET',
            url: 'http://localhost:8080/api/users/' + vkId,
            dataType: 'json',
            success: function(data){
                console.log ( 'Success: ' + QueriesActions.GET_USER_SUCCESS );
                dispatch({
                    type: QueriesActions.GET_USER_SUCCESS,
                    payload: data
                });
            },
            error: function(error){
                console.log ( 'Fail: ' + JSON.str);
                dispatch({
                    type: QueriesActions.GET_USER_FAIL
                });
            }
        });
    };
}

export function getFriends(vkId){
    return (dispatch) => {
        dispatch({
            type: QueriesActions.GET_FRIENDS_REQUEST
        });

        $.ajax({
            method: 'GET',
            url: 'http://localhost:8080/api/users/' + vkId + '/friends',
            dataType: 'json',
            success: function(data){
                console.log ( 'Success: ' + QueriesActions.GET_FRIENDS_SUCCESS );
                dispatch({
                    type: QueriesActions.GET_FRIENDS_SUCCESS,
                    payload: {vkId: vkId, friends: data}
                });
            },
            error: function(error){
                console.log ( 'Fail: ' + JSON.str);
                dispatch({
                    type: QueriesActions.GET_FRIENDS_FAIL
                });
            }
        });
    };
}