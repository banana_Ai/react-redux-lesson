import {GET_OAUTH_LINK_SUCCESS, GET_OAUTH_LINK_REQUEST, GET_OAUTH_LINK_FAIL} from '../constants/User'
var $ = require('jquery/src/core');
require('jquery/src/ajax');
require('jquery/src/ajax/xhr');

export function getOAuthLink(){
	return (dispatch) => {
		dispatch({
			type: GET_OAUTH_LINK_REQUEST
		});

		$.ajax({
			method: 'GET',
			url: 'http://localhost:8080/api/login/link',
			dataType: 'json',
			success: function(data){
				console.log ( 'Success: ' + GET_OAUTH_LINK_SUCCESS );
				dispatch({
					type: GET_OAUTH_LINK_SUCCESS,
					payload: data.link
				});
			},
			error: function(error){
				console.log ( 'Fail: ' + JSON.str);
				dispatch({
					type: GET_OAUTH_LINK_FAIL
				});
			}
		});
	};
}