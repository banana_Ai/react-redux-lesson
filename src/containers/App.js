import React, {Component, PropTypes} from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import User from '../components/User'
import Graphs from '../components/Graphs'
import * as userActions from '../actions/UserActions'
import * as queriesActions from '../actions/GraphsActions'

class App extends Component {
	render() {
		return <div>
			<User user={this.props.user} userActions={this.props.userActions}/>
			<Graphs graphs={this.props.graphs} queriesActions={this.props.queriesActions}/>
		</div>
	}
}

function mapStateToProps(state) {
	return {
		user: state.user,
        graphs: state.graphs
	};
}

function mapDispatchToProps(dispatch) {
	return {
		userActions: bindActionCreators(userActions, dispatch),
		queriesActions: bindActionCreators(queriesActions, dispatch),
	}
}


export default connect(mapStateToProps, mapDispatchToProps)(App)