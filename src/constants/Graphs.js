import {Enum} from 'enumify';

export default class GraphsActions extends Enum {}
GraphsActions.initEnum([
    'GET_USER_REQUEST',
    'GET_USER_SUCCESS',
    'GET_USER_FAIL',

    'GET_FRIENDS_REQUEST',
    'GET_FRIENDS_SUCCESS',
    'GET_FRIENDS_FAIL'
]);